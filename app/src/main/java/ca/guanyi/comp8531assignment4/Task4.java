package ca.guanyi.comp8531assignment4;

import android.util.Log;
import android.os.Process;

/**
 * Created by Guanyi on 6/21/2017.
 */

public class Task4 extends Thread {

    private int fib = 0;
    private int priority;

    public Task4(int fib, int priority) {
        this.fib = fib;
        this.priority = priority;
    }

    public void run() {
        Process.setThreadPriority(priority);
        long startTime = System.currentTimeMillis();
        new SharedJob().fibonacci(fib);
        long endTime = System.currentTimeMillis();
        Log.i("Result", "Task 4 Elapsed time is " + ((endTime - startTime)/1000) + " " +
                "seconds");
    }
}
