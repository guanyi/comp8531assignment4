package ca.guanyi.comp8531assignment4;

import android.os.Process;
import android.util.Log;

/**
 * Created by Guanyi on 6/21/2017.
 */

public class Task3 extends Thread {
    private int fib = 0;
    private int priority;

    public Task3(int fib, int priority) {
        this.fib = fib;
        this.priority = priority;
    }

    public void run() {
        Process.setThreadPriority(priority);
        long startTime = System.currentTimeMillis();
        new SharedJob().fibonacci(fib);
        long endTime = System.currentTimeMillis();
        Log.i("Result", "Task 3 Elapsed time is " + ((endTime - startTime)/1000) + " " +
                "seconds");
    }
}
