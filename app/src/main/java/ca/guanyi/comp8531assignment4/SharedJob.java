package ca.guanyi.comp8531assignment4;

/**
 * Created by Guanyi on 6/21/2017.
 */

public class SharedJob {

    public int fibonacci (int x) {
        if (x <= 1) {
            return 1;
        }
        return fibonacci(x - 1) + fibonacci(x - 2);
    }
}
