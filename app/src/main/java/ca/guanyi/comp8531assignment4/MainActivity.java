package ca.guanyi.comp8531assignment4;

import android.os.Process;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tv = (TextView) findViewById(R.id.resultView);
    }

    @Override
    protected void onResume() {
        super.onResume();  // Always call the superclass method first
        int fib = 44;
        Task1 t1 = new Task1(fib, Process.THREAD_PRIORITY_FOREGROUND);
        Task2 t2 = new Task2(fib, Process.THREAD_PRIORITY_FOREGROUND);
        Task3 t3 = new Task3(fib, Process.THREAD_PRIORITY_FOREGROUND);
        Task4 t4 = new Task4(fib, Process.THREAD_PRIORITY_BACKGROUND);
        Task5 t5 = new Task5(fib, Process.THREAD_PRIORITY_BACKGROUND);
        Task6 t6 = new Task6(fib, Process.THREAD_PRIORITY_BACKGROUND);
        Task7 t7 = new Task7(fib, Process.THREAD_PRIORITY_BACKGROUND);
        Task8 t8 = new Task8(fib, Process.THREAD_PRIORITY_BACKGROUND);
        Task9 t9 = new Task9(fib, Process.THREAD_PRIORITY_BACKGROUND);
        Task10 t10 = new Task10(fib, Process.THREAD_PRIORITY_BACKGROUND);
        Task11 t11 = new Task11(fib, Process.THREAD_PRIORITY_BACKGROUND);
        Task12 t12 = new Task12(fib, Process.THREAD_PRIORITY_BACKGROUND);

        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
        t7.start();
        t8.start();
        t9.start();
        t10.start();
        t11.start();
        t12.start();
    }
}
